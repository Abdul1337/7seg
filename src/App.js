import React from 'react';
import './App.css';
function App() {
  return (
    <div className="Display7Seg">
      <SevenSeg num="0" delay="1000"></SevenSeg>
    </div>
  );
}



class SevenSeg extends React.Component{
  segMap = {
    "0" : get8bitBinary(119),
    "1" : get8bitBinary(18),
    "2" : get8bitBinary(93),
    "3" : get8bitBinary(91),
    "4" : get8bitBinary(58),
    "5" : get8bitBinary(107),
    "6" : get8bitBinary(111),
    "7" : get8bitBinary(82),
    "8" : get8bitBinary(127),
    "9" : get8bitBinary(123),
  }
  constructor(props){
    super(props);
    this.state = {
      num : this.props.num,  
    }
  }
  getStyle(n){
    return parseInt(this.segMap[this.state.num][n+1])
      ?{
        background : "#000",
        animation : "bringIn 0.4s linear forwards"
      }
      :{
        background : "#000",
        animation : "bringOut 0.4s linear forwards"

      };
  }
  componentDidMount(){
    setInterval(()=>{
      this.setState({
        num : parseInt(this.state.num) > 8 ? "0" : parseInt(this.state.num) + 1,
      })
    },this.props.delay)
  }
  render(){
  console.log(Date.now());    
    return(
      <div className="Container">
        <div className="row">
          <div className="horizontal" style={this.getStyle(0)} key="seg1"></div>
        </div>
        <div className="row">
          <div className="vertical" style={this.getStyle(1)} key="seg2"></div>
          <div className="vertical" style={this.getStyle(2)} key="seg3"></div>
        </div>
        <div className="row">
          <div className="horizontal" style={this.getStyle(3)} key="seg4"></div>
        </div>
        <div className="row">
        <div className="vertical" style={this.getStyle(4)} key="seg5"></div>
          <div className="vertical" style={this.getStyle(5)} key="seg6"></div>
        </div>
        <div className="row">
          <div className="horizontal" style={this.getStyle(6)} key="seg7"></div>
        </div>
      </div>
    )
  }
}

function get8bitBinary(num){
  return ("00000000"+num.toString(2)).slice(-8);
}




export default App;
